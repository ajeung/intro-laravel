<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function form(Request $request){
        $first = $request->firstName;
        $last = $request->lastName; 

        return view('welcome', compact('first', 'last'));
    }
}
